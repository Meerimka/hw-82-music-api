const mongoose = require('mongoose');
const nanoid =require('nanoid');
const Artist =require('./models/Artist');
const Album =require('./models/Album');
const Track =require('./models/Track');
const User =require('./models/User');
const config = require('./config');



const run =async () => {
  await  mongoose.connect(config.dbUrl, config.mongoOptions);

  const connection = mongoose.connection;

  const collections = await connection.db.collections();

  for(let collection of collections){
      await collection.drop();
  }

  const [user, admin] = await User.create({
    username: 'user',
      password: '123',
      role: 'user',
      displayName:'User',
      avatar: 'User.jpg',
      token: nanoid()
  },
      {
          username: 'admin',
          password: '123',
          role: 'admin',
          displayName:'Admin',
          avatar: 'User.jpg',
          token: nanoid()
      });

const [artist, artist2,artist3] = await Artist.create(
    {
        title: "Eminem",
        image: "eminem.jpg",
        published: 'true',
        user: user._id,
        info: "Marshall Bruce Mathers III (born October 17, 1972), known professionally as Eminem"
    },
    {
        title: "Logic",
        image: 'logic.jpg',
        published: 'true',
        user: user._id,
        info: 'Lolkek'
    },
    {
        title: "Linkin park",
        image: 'linkin.jpg',
        published: 'true',
        user: user._id,
        info: 'Linkin Park is an American rock band from Agoura Hills, California. The band\'s current lineup comprises vocalist/rhythm guitarist Mike Shinoda, lead guitarist Brad Delson, bassist Dave Farrell, DJ/keyboardist Joe Hahn, and drummer Rob Bourdon, all of whom are founding members. Former members include bassist Kyle Christner and vocalists Mark Wakefield and Chester Bennington, the latter being a member until his passing in 2017.'
    },
);

    const  [album, album2, album3,album4] = await Album.create(
        {
            title:'Recovery',
            artist: artist._id,
            production: '2017',
            published: 'false',
            user: user._id,
            image:"recovery.jpg"

        },
        {
            title: "Boby tarantino",
            artist: artist2._id,
            production: '2016',
            image: 'logic.jpg',
            published: 'true',
            user: admin._id
        },
        {
            title: "A Thousand Suns",
            artist: artist3._id,
            production: '2010',
            published: 'true',
            user: user._id,
            image: 'thousand.jpg'
        },
    {
        title: "Living things",
            artist: artist3._id,
        production: '2012',
        published: 'true',
        user: user._id,
        image: 'livingthings.jpg'
    }
    );

    const  tracks = await Track.create(
        {
            title:'Not Afraid',
            album: album._id,
            duration: "4:18",
            published: 'true',
            number: 1,
            youtubeSrc: 'https://www.youtube.com/embed/j5-yKhDd64s'

        },
        {
            title: 'Flexecution',
            album: album2._id,
            duration: '4:00',
            published: 'false',
            number: 1,
            youtubeSrc: 'https://www.youtube.com/embed/M2NIMHVmGwk'
        },
        {
            title: 'The Jam',
            album: album2._id,
            duration: '4:00',
            published: 'true',
            number: 2,
            youtubeSrc: 'https://www.youtube.com/embed/ZEVHvy9Jo7A'
        },
        {
            title: 'Wrist',
            album: album2._id,
            duration: '4:22',
            published: 'true',
            number: 3,
            youtubeSrc: 'https://www.youtube.com/embed/i-YykLXxTzk'
        },
        {
            title: '44 Bars',
            published: 'true',
            album: album2._id,
            duration: '4:05',
            number: 4,
            youtubeSrc: 'https://www.youtube.com/embed/anI5b2PEmdA'
        },
        {
            title: 'Deeper Than Money',
            album: album2._id,
            published: 'true',
            duration: '3:87',
            number: 5,
            youtubeSrc: 'https://www.youtube.com/embed/iKC7lzRa6to'
        },
        {
            title: 'The Catalyst',
            album: album3._id,
            published: 'true',
            duration: '4:44',
            number: 1,
            youtubeSrc: 'https://www.youtube.com/embed/51iquRYKPbs'
        },
        {
            title: 'The Requiem',
            album: album3._id,
            duration: '2:01',
            published: 'true',
            number: 2,
            youtubeSrc: 'https://www.youtube.com/embed/M4KSZQYmuMQ'
        },
        {
            title: 'The Radiance',
            album: album3._id,
            duration: '0:57',
            published: 'true',
            number: 3,
            youtubeSrc: 'https://www.youtube.com/embed/m4AI6kHHyWs'
        },
        {
            title: 'Empty Spaces',
            album: album3._id,
            duration: '5:17',
            number: 4,
            youtubeSrc: 'https://www.youtube.com/embed/9fn8gXGuYEM'
        },
        {
            title: 'In My Remains',
            album: album4._id,
            published: 'true',
            duration: '3:20',
            number: 1,
            youtubeSrc: 'https://www.youtube.com/embed/QLFiuNdQrzI'
        },
        {
            title: 'Burn It Down',
            album: album4._id,
            published: 'true',
            duration: '3:52',
            number: 2,
            youtubeSrc: 'https://www.youtube.com/embed/dxytyRy-O1k'
        },
        {
            title: 'Lies Greed Misery',
            album: album4._id,
            duration: '2:27',
            published: 'true',
            number: 3,
            youtubeSrc: 'https://www.youtube.com/embed/anI5b2PEmdA'
        },
    );

  return connection.close();

};


run().catch(error =>{
    console.log('Something wrong happened ...' ,error);
});

