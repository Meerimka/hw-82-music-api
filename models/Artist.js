const mongoose =require('mongoose');

const Schema = mongoose.Schema;


const ArtistSchema = new Schema({
    title:{
        type:String,
        required: true,
    },
    published: {
        type: Boolean,
        required: true,
        default: false
    },
    image: String,
    info: String,
    user: {
        type: Schema.ObjectId,
        ref: 'User',
        required: true
    }
});

const  Artist = mongoose.model('Artist', ArtistSchema);


module.exports = Artist;
