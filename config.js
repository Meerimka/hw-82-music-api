const path = require('path');

const rootPath = __dirname;

module.exports = {
  rootPath,
  uploadPath: path.join(rootPath, 'public/uploads'),
  dbUrl: 'mongodb://localhost/music',
  mongoOptions: {
    useNewUrlParser: true,
    useCreateIndex: true
  },
  facebook:{
    appId:'1330194153811870',
    appSecret: '80817bb6093e4a8d21f9dc4e0333e153'
  }
};
