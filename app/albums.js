const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');
const tryAuth = require('../middlewere/tryAuth');
const auth = require('../middlewere/auth');
const permit = require('../middlewere/permit');

const Album = require('../models/Album');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/',tryAuth, async (req, res) => {

    try {
    let criteria = {published: true};
    if(req.user && req.user.role === 'user'){
        criteria = {
            $or:[
                {published: true},
                {user:req.user._id}
            ]
        }
    }

    if(req.user && req.user.role === 'admin'){
        criteria = {}
    }

    if(req.query.artist){
        if(req.user && req.user.role === 'user'){
            console.log("ss");
            criteria = {
                artist: req.query.artist,
                $or:[
                    {published: true},
                    {user:req.user._id}
                ]
            }
        }
        if(req.user && req.user.role === 'admin'){
            criteria = {artist: req.query.artist}
        }
        criteria = {
            artist:req.query.artist,
            $or:[
                {published: true}
            ]
        };
    }


    await Album.find(criteria).populate('artist').sort({"production": 1}).then(result => res.send(result));

}catch (e) {
    return res.status(500).send(e);
}

});

router.get('/:id', (req, res) => {
    Album.findById(req.params.id).populate('artist')
        .then(album => {
            if (album) res.send(album);
            else res.sendStatus(404);
        })
        .catch(() => res.sendStatus(500));
});

router.post('/',[ auth, upload.single('image')],async (req, res) => {
    try {
        const album =  new Album({
            title: req.body.title,
            artist:req.body.artist,
            user: req.user._id,
            image: req.file.filename,
            production: req.body.production
        });
        await album.save();
        res.send(album);
    }catch (e) {
        if(e.name === 'ValidationError'){
            return res.status(400).send(e);
        }
        return res.status(500).send(e);
    }
});


router.post('/:id/toggle_published',[auth, permit('admin')], async (req, res)=>{
    const album = await Album.findById(req.params.id);

    if(!album){
        return res.sendStatus(404);
    }

    album.published = !album.published;

    await album.save();
    return res.send(album);

});


module.exports = router;
