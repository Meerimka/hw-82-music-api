const express = require('express');
const Artist = require('../models/Artist');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');
const auth = require('../middlewere/auth');
const permit = require('../middlewere/permit');
const tryAuth = require('../middlewere/tryAuth');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});
const router = express.Router();

router.get('/',tryAuth,async (req,res)=>{
    try {
        let criteria = {published: true};

        if(req.user){
            criteria = {
                $or:[
                    {published: true},
                    {user:req.user._id}
                ]
            }
        }

        const artists = await Artist.find(criteria);

        return res.send(artists);
    }catch (e) {
        return res.status(500).send(e);
    }

});
 
router.post('/' , auth ,upload.single('image') , async (req,res)=>{
    try {
        const artist = new Artist({
            title: req.body.title,
            user: req.user._id,
            image: req.file.filename,
            info: req.body.info
        });

        await artist.save();

        res.send(artist);
        
    }catch (e) {
        if(e.name === 'ValidationError'){
            return res.status(400).send(e);
        }
        return res.status(500).send(e);
    }
});

router.post('/:id/toggle_published',[auth, permit('admin')], async (req, res)=>{
    const artist = await Artist.findById(req.params.id);

    if(!artist){
        return res.sendStatus(404);
    }

    artist.published = !artist.published;

    await artist.save();
    return res.send(artist);



});


module.exports =router;
