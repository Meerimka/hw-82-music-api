const express= require('express');
const axios = require('axios');
const nanoid = require('nanoid');
const multer = require('multer');
const path = require('path');
const config = require('../config');
const User = require('../models/User');
const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});


    router.post('/',upload.single('avatar'),async (req,res)=>{
        try {
            let filename = 'User.jpg';
            if (req.file && req.file.filename) {
                filename = req.file.filename;
            }
                const user = new User ({
                username:req.body.username,
                password: req.body.password,
                avatar: filename,
                displayName: req.body.displayName
            });

            user.generateToken();
          await user.save();
          return res.send({token:user.token})
        } catch(error){
            console.log(error);
            res.status(400).send(error)
        }
    });



    router.post('/sessions', async (req, res)=>{
        const user = await User.findOne({username:req.body.username});

        if(!user){
            return res.status(400).send({error:'User does not exist '});
        }

        const isMatch = await user.checkPassword(req.body.password);

        if(!isMatch){
            return res.status(400).send({error: 'Password incorrect'});
        }
        user.generateToken();
        await user.save();
        res.send(user);
    });


    router.put('/', async (req, res) => {
        const token = req.get('Authorization');

        if (!token) {
            return res.status(401).send({error: 'Authorization headers not present'});
        }

        const user = await User.findOne({token});

        if (!user) {
            return res.status(401).send({error: 'Token incorrect'});
        }

        user.password = req.body.password;

        await user.save();

        res.sendStatus(200);
    });

    router.post('/facebookLogin', async (req, res) => {

        const inputToken = req.body.accessToken;
        const accessToken = config.facebook.appId + '|'+ config.facebook.appSecret;
        const debugTokenUrl = `https://graph.facebook.com/debug_token?input_token=${inputToken}&access_token=${accessToken}`;

        try {
            const response = await axios.get(debugTokenUrl);

            const responseData = response.data;

            if(responseData.data.error){
                return res.status(500).send({error:'Token incorrect'})
            }

            if(responseData.data.user_id !== req.body.id){
                return res.status(500).send({error:'User is wrong'})
            }

            let user = await User.findOne({facebookId: req.body.id});

            if(!user){
                user = new User({
                    username:req.body.email || req.body.id,
                    password: nanoid(),
                    facebookId: req.body.id,
                    avatar: req.body.picture.data.url,
                    displayName: req.body.name
                })
            }

            user.generateToken();

            await user.save();

            return res.send({message: 'Login or registered successful!', user});

        }catch (e) {
            console.log(e);
            return res.status(500).send({error:'Something went wrong'})
        }
    });


    

router.delete('/sessions', async (req, res)=>{
    const token = req.get('Token');
    const success = {message: 'Ok'};


    if (!token) {
        return res.send(success);
    }

    const user = await User.findOne({token});

    if (!user) {
        return res.send(success);
    }

    user.generateToken();
    await user.save();

    res.send(success);

});

module.exports = router;