const  express = require('express');
const Track = require('../models/Track');
const nanoid = require('nanoid');
const multer = require('multer');
const path = require('path');
const config = require('../config');
const tryAuth = require('../middlewere/tryAuth');
const auth = require('../middlewere/auth');
const permit = require('../middlewere/permit');


const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});


const router = express.Router();

router.get('/',tryAuth,async (req,res)=>{
    try {
        let criteria = {published: true};
        if(req.user && req.user.role === 'user'){
            criteria = {
                $or:[
                    {published: true},
                    {user:req.user._id}
                ]
            }
        }

        if(req.user && req.user.role === 'admin'){
            criteria = {}
        }

        if(req.query.album){
            if(req.user && req.user.role === 'user'){
                criteria = {
                    album: req.query.album,
                    $or:[
                        {published: true},
                        {user:req.user._id}
                    ]
                }
            }
            if(req.user && req.user.role === 'admin'){
                criteria = {album: req.query.album}
            }
            criteria = {
                album: req.query.album,
                $or:[
                    {published: true}
                ]
            };
            await Track.find({album: req.query.album}).populate('album').sort({number: 1}).then(result => res.send(result))
        }
       await Track.find().sort({number: 1}).populate('album').then(track => res.send(track))

    }catch (e) {
        return res.status(500).send(e);
    }

});

router.post('/', upload.single('image'), (req, res) => {
    const trackData = req.body;

    if (req.file) {
        trackData.image = req.file.filename;
    }

    const album = new Track( trackData);

    album.save()
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error));
});


module.exports =router;